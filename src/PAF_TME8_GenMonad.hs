
module PAF_TME8_GenMonad where

{-{

# TME 8 : Générateur aléatoire monadique de QuickCheck

Dans ce TME nous étudions la définition de générateurs aléatoires pour
le test basé sur les propriétés et QuickCheck. Ce thème est en rapport
avec le cours puisque l'architecture des générateurs de QuickCheck est
basée sur le concept de monade.

**Attention** : nous ne faisons quasiment pas de *property-based testing* dans
 ce TME, nous nous intéressons principalement à la définition de générateurs
 *custom* (le TME4 explique la partie test proprement dite).

Une fois n'est pas coutûme ce TME est plutôt du type tutoriel.
Le principe est de répondre aux différents **questions** et également,
d'évaluer dans GHCI (une fois le module chargé) tous les exemples
d'interactions  (vous pouvez remplacer les `???????` par les exemples
de valeurs obtenues). Comme nous travaillons sur des générateurs aléatoires,
 on obtient des valeurs potentiellement différentes à chaque exécution,
 donc il est utile d'exécuter plusieurs fois les mêmes interactions.

Pour ce TME, nous allons utiliser directement les définitions de
la bibliothèque QuickCheck.


}-}

import Test.QuickCheck
import System.Random (Random)
import Control.Applicative (liftA2)

{-{

# Première Partie : Architecture des générateurs QuickCheck

L'architecture des générateurs de QuickCheck est basée le type `Gen a` défini
de la façon suivante :

```haskell
>>> :info Gen
newtype Gen a
  = Test.QuickCheck.Gen.MkGen {Test.QuickCheck.Gen.unGen :: Test.QuickCheck.Random.QCGen
                                                            -> Int -> a}
  	-- Defined in ‘Test.QuickCheck.Gen’
instance [safe] Applicative Gen -- Defined in ‘Test.QuickCheck.Gen’
instance [safe] Functor Gen -- Defined in ‘Test.QuickCheck.Gen’
instance [safe] Monad Gen -- Defined in ‘Test.QuickCheck.Gen’
instance [safe] Testable prop => Testable (Gen prop)
  -- Defined in ‘Test.QuickCheck.Property’
```

L'information la plus importante est que `Gen` est un contexte monadique (et donc applicatif et
fonctoriel). L'intuition principale est que les générateurs se combinent essentiellement
avec *bind*, et donc que la notation `do` est disponible. Nous allons y revenir mais nous
allons d'abord exploiter la disponibilité d'un certain nombre de générateurs par défaut.

## Générateurs par défaut avec Arbitrary

Une autre composante importante de la génération aléatoire avec QuickCheck est
la *typeclasse* `Arbitrary`.

```haskell
>>> :info Arbitrary
class Arbitrary a where
  arbitrary :: Gen a
  shrink :: a -> [a]
  {-# MINIMAL arbitrary #-}
  	-- Defined in ‘Test.QuickCheck.Arbitrary’
```

La fonction générique `arbitrary` peut être vue comme le générateur
par défaut pour le type `a`.   La fonction `shrink` implémente
(lorsque cela a du sens) un algorithme de réduction de donnée qui propose
une liste de valeurs «plus petites» à partir d'une valeur initiale.
Nous n'aborderons pas cette aspect dans le TME, mais c'est ce qui permet
à QuickCheck de trouver des contre-exemples les plus petits possibles
en cas de propriété invalidée.

Il existe de nombreuses instances de `arbitrary` prédéfinies, comme
par exemple des générateurs (et *shrinkers*) pour les types de base :

```haskell
instance Arbitrary Bool
instance Arbitrary Char
instance Arbitrary Integer
instance Arbitrary Int
instance Arbitrary Float
instance Arbitrary Double
```

Pour tester ces générateurs, nous pouvons utiliser les fonctions
`sample` ou `sample'` dont les signatures sont les suivantes :

```haskell
sample :: Show a => Gen a -> IO ()
sample' :: Gen a -> IO [a]
```

La première affiche la série de valeurs choisies arbitrairement,
 et la seconde retourne une liste. Toutes les deux ne peuvent
être utilisées que dans le contexte de `IO` car le générateur
aléatoire (de nombre) par défaut n'est pas pur (il utilise
une graine qui change à chaque initialisation du générateur).

Dans GHCI on peut par exemple écrire les expressions suivantes (après avec chargé le
 module `PAF_TME8_GenMonad`, ou directement avec `stack ghci`)  :

```haskell
>>> sample' (arbitrary :: Gen Integer)
[0,0,3,3,6,7,7,-8,3,-7,11]

>>> sample' (arbitrary :: Gen Integer)
[0,2,-2,-3,2,-10,-2,14,-2,-5,20]

>>> sample' (arbitrary :: Gen Char)
"\393105\DC1h\839920n\GSY?\758572\765624\516695"

-- >>> sample' (arbitrary :: Gen Char)
-- "Z\1082697+\304938C\609601\557853\&9\246852\US\SUB"
```

Bien sûr, les résultats changent à chaque appel, ce sont des actions et
non des fonctions pures.

Il y a aussi des générateurs pour les types polymorphes de base :

```haskell
instance Arbitrary a => Arbitrary (Maybe a)
instance Arbitrary a => Arbitrary [a]
instance (Arbitrary a, Arbitrary b) => Arbitrary (Either a b)
instance (Arbitrary a, Arbitrary b) => Arbitrary (a, b)
instance (Arbitrary a, Arbitrary b, Arbitrary c) => Arbitrary (a, b, c)
-- etc ... jusqu'aux 10-uplets !
```

La fonction `sample'` retourne des listes de 11 données, ce qui est parfois
une peu trop gros, donc nous allons plutôt utiliser la fonction suivante :

}-}

samples :: Int -> Gen a -> IO [a]
samples n gen = do
  l <- sample' gen
  return $ take n l

{-{

Par exemple :

```haskel
>>> samples 4 (arbitrary :: Gen [Integer])
[[],[0],[-1,-3,3,0],[-5,-2,4]]

>>> samples 10 (arbitrary :: Gen (Maybe Int))
[Just 0,Nothing,Just (-1),Just 4,Nothing,Nothing,Just (-9),Just (-7),Just (-7),Just 9]

>>> samples 3 (arbitrary :: Gen (Double, Bool))
[(0.0,True),(-1.773966838787001,True),(-0.23400671049402483,False)]

}-}

{-{

**Question :** : donner une expression avec `samples` permettant de générer des données
de la forme suivante :

```haskell
[[],[Left (-1),Left 2],[Right Nothing,Left 1,Right (Just False),Left 0]]
```

}-}


-- REPONDRE ICI
reponse = samples 4 (arbitrary :: Gen [Either Int (Maybe Bool)])

{-{

# Générateurs custom

Nous allons maintenant nous intéresser à la définition proprement dite
de générateurs.

Pour cela, nous allons étudier un certain nombre de *combinateurs monadiques* de
génération. On a tout d'abord quelques générateurs que l'on pourrait appeler atomiques.

## Générateurs atomiques.

Le principe de génération élémentaire est un générateur uniforme d'entiers, que
l'on peut simplement utiliser pour générer des valeurs des types numériques de bases,
 ainsi que des types énumérés (comme `Bool` ou `Char`).
Ces générateurs atomiques sont fournis par le module `System.Random`
 de la bibliothèque *random* (cf. <https://hackage.haskell.org/package/random-1.1/docs/System-Random.html>). 

```haskell
>>> :info Random
unknown command 'info'

La fonction de quickcheck permettant de transformer une instance de
`Random` en un générateur est la suivante :

```haskell
choose :: Random a => (a, a) -> Gen a
```

Le premier argument est un couple `(m, n)` correspondant à un intervalle avec
`m` et `n` inclus:

```haskell
>>> sample' $ choose ((4, 10) :: (Integer,Integer))
[10,6,5,6,9,6,10,7,7,8,5]

>>> sample' $ choose ('a', 'z')
"swebvydojtg"

}-}


{-{

**Question** : Définir un générateur avec la signature suivante :

```haskell
chooseNat :: (Random a, Num a) => a -> Gen a
```
tel que `genNat n` ne génère que des nombres entre 1 et `n`

}-}

chooseNat :: (Random a, Num a) => a -> Gen a
chooseNat n = choose (1,n)

{-{

```haskell
>>> sample' $ chooseNat (10::Integer)
[1,1,6,6,7,4,3,10,2,1,8]

}-}

{-{

## Combinateurs élémentaires

En complément des générateurs atomiques on trouve des opérateurs assez simples permettant de composer des générateurs atomiques.

Par exemple, la fonction `elements :: [a] -> Gen a` retourne un générateur constant
pour une valeur prise au hasard dans la liste spécifiée.

```haskell
>>> sample' $ elements [42, 2, 17]
???????

>>> sample' $ elements ['a' .. 'e']
???????
```
}-}

{-{

Le générateur `listOf :: Gen a -> Gen [a]` génère une liste de `a`.

```haskell
>>> samples 5 $ listOf (chooseNat (10::Integer)) 
[[],[],[6],[10,3],[7,6,4,6,9,9,7,1]]

La taille de la liste générée est bornée par un paramètre `size` qui
 est passé (via *bind* nous allons y revenir) entre générateurs.
Il est possible d'influencer sur la taille avec :

```haskell
resize :: Int -> Gen a -> Gen a
```

Par exemple :

```haskell
>>> samples 3 $ resize 10 $ listOf $ chooseNat (10::Integer)
[[],[],[3]]

Les deux premières listes sont de petite taille, mais la 3ème
est bien de longueur 10. En fait, l'indicateur de taille est
plus une sorte de demande informelle qu'un information à prendre
précisément en compte. Pour de nombreux générateurs prédéfinis,
 cette taille est prise comme une borne supérieure non-stricte,
 et les premières valeurs générées sont souvent de petite taille.
On verra un peu plus loin comment contrôler la taille un peu plus
finement.

Un autre générateur utile est `oneof:: [Gen a] -> Gen a`.
Il s'agit de choisir, de façon arbitrarire, un générateur parmi
la liste proposée. La contrainte est que tous les générateurs
sont pour le même type `a`.

Par exemple :

```haskell
>>> sample' $ oneof [choose (1, 10), choose (100, 110), choose (1000, 1010)]
Ambiguous type variable ‘a0’ arising from a use of ‘choose’
prevents the constraint ‘(Random a0)’ from being solved.
Relevant bindings include
  it :: IO [a0]
    (bound at E:\study\M1S2\PAF\tme8\paf-tme8-monades\src\PAF_TME8_GenMonad.hs:299:2)
Probable fix: use a type annotation to specify what ‘a0’ should be.
These potential instances exist:
  instance Random CChar -- Defined in ‘System.Random’
  instance Random CDouble -- Defined in ‘System.Random’
  instance Random CFloat -- Defined in ‘System.Random’
  ...plus 33 others
  (use -fprint-potential-instances to see them all)
Ambiguous type variable ‘a0’ arising from the literal ‘1’
prevents the constraint ‘(Num a0)’ from being solved.
Relevant bindings include
  it :: IO [a0]
    (bound at E:\study\M1S2\PAF\tme8\paf-tme8-monades\src\PAF_TME8_GenMonad.hs:299:2)
Probable fix: use a type annotation to specify what ‘a0’ should be.
These potential instances exist:
  instance Num a => Num (Blind a)
    -- Defined in ‘Test.QuickCheck.Modifiers’
  instance Num a => Num (Fixed a)
    -- Defined in ‘Test.QuickCheck.Modifiers’
  instance Num a => Num (Large a)
    -- Defined in ‘Test.QuickCheck.Modifiers’
  ...plus 68 others
  (use -fprint-potential-instances to see them all)

Un opérateur plus général permet de donner plus ou moins de poids à un
générateur. Le poids est indiqué par un entier, et bien sûr une valeur
plus grande augment la probabilité d'utiliser le générateur correspondant.

La signature de cette fonction est la suivante :

```haskell
frequency :: [(Int, Gen a)] -> Gen a
```

**Attention**: il ne faut pas voir ici un contrôle fin de la distribution
aléatoire, car cette dernière n'est pas maîtrisable du fait de l'enchaînement
de générateurs arbitraires. On fait ici plus de la génération *arbitraire* que
de la *véritable* génération aléatoire (avec contrôle de la distribution des
valeurs générées).

Voici une variante de l'exemple précédent :

```haskell
>>> sample' $ frequency [(60, choose (1, 10)), (30, choose (100, 110)), (10, choose (1000, 1010))]
Ambiguous type variable ‘a0’ arising from a use of ‘choose’
prevents the constraint ‘(Random a0)’ from being solved.
Relevant bindings include
  it :: IO [a0]
    (bound at E:\study\M1S2\PAF\tme8\paf-tme8-monades\src\PAF_TME8_GenMonad.hs:347:2)
Probable fix: use a type annotation to specify what ‘a0’ should be.
These potential instances exist:
  instance Random CChar -- Defined in ‘System.Random’
  instance Random CDouble -- Defined in ‘System.Random’
  instance Random CFloat -- Defined in ‘System.Random’
  ...plus 33 others
  (use -fprint-potential-instances to see them all)
Ambiguous type variable ‘a0’ arising from the literal ‘1’
prevents the constraint ‘(Num a0)’ from being solved.
Relevant bindings include
  it :: IO [a0]
    (bound at E:\study\M1S2\PAF\tme8\paf-tme8-monades\src\PAF_TME8_GenMonad.hs:347:2)
Probable fix: use a type annotation to specify what ‘a0’ should be.
These potential instances exist:
  instance Num a => Num (Blind a)
    -- Defined in ‘Test.QuickCheck.Modifiers’
  instance Num a => Num (Fixed a)
    -- Defined in ‘Test.QuickCheck.Modifiers’
  instance Num a => Num (Large a)
    -- Defined in ‘Test.QuickCheck.Modifiers’
  ...plus 68 others
  (use -fprint-potential-instances to see them all)

On aurait ici, *grosso modo*, 60% de chance de tirer un nombre entre 1 et 19, 30% entre 100 et 110 et 10% entre 1000 et 1010.

Voici une petite vérification «maison» de cette distribution attendue :

}-}

genFreq :: Gen Integer
genFreq = frequency [(60, choose (1, 10)), (30, choose (100, 110)), (10, choose (1000, 1010))]

freqStats :: (Num a, Ord a) => [a] -> (Double, Double, Double)
freqStats xs =
  aux 0 0 0 xs
  where aux nb1 nb2 nb3 [] =
          let tot = nb1 + nb2 + nb3
              coef = 100.0 / (fromIntegral tot)
          in (fromIntegral nb1 * coef, fromIntegral nb2 * coef, fromIntegral nb3 * coef)
        aux nb1 nb2 nb3 (x:xs) | x <= 10 = aux (nb1+1) nb2 nb3 xs
                               | x <= 110 = aux nb1 (nb2+1) nb3 xs
                               | otherwise = aux nb1 nb2 (nb3+1) xs

checkFrequency :: (Random a, Num a, Ord a) => Gen [a] -> IO (Double, Double, Double) 
checkFrequency gen = do
  xs <- generate gen
  return $ freqStats xs

{-{

Essayez d'invoquer plusieurs fois cette fonction de vérification de la fréquence.

```haskell
>>> checkFrequency $ resize 100000 $ listOf genFreq
(59.58205086667746,30.179815324801556,10.238133808520978)

On obtient des résultats plutôt cohérents avec la spécification, mais tout cela dépend des générateurs utilisés.

}-}

{-{

Le dernier générateur que l'on présente permet de filtrer des valeurs en entrées, par un principe
de *génération avec rejet*. L'idée est très simple : on prend des valeurs en entrées et on ne conserve
que celle qui vérifie une propriété. Le combinateur concerné possède la signature suivante :

```haskell
suchThat :: Gen a -> (a -> Bool) -> Gen a
```

Si l'on veut par exemple uniquement générer des entiers pairs, on pourra utiliser l'expression
suivante :

```haskell
>>> sample' $ chooseNat (100::Integer) `suchThat` even
[40,100,30,36,72,74,64,4,74,52,10]

**Attention** : il ne faut pas abuser du `suchThat`   (que l'on utilise le plus souvent en
position infixe, pour des questions de lisibilité) car les valeurs rejetées peuvent être nombreuses.
En particulier, il faut être à peu près certain que le prédicat concerné est «souvent» vrai pour
le générateur qu'il filtre. Voici un bon contre-exemple :

```haskell
>>> sample' $ chooseNat (100000::Integer) `suchThat` (==1)
[1,1,1,1,1,1,1,1,1,1,1]

Ici, *QuickCheck* a bien retourné une liste de 1 mais après un temps assez long,
 et l'exemple suivant ne s'arrête bien sûr jamais (ne pas le lancer !) :

```haskell
>>> sample' $ choose ((1, 10) ::(Integer,Integer)) `suchThat` (==11)
ProgressCancelledException

Il existe d'autres combinateurs intéressants dans la bibliothèque fournie par *QuickCheck*,
 on n'hésitera pas à consulter la documentation associée (rubrique *Generator combinators*).

}-}


{-{

## Combinateurs monadiques

On peut créer des générateurs intéressants avec les générateurs atomiques et
les combinateurs élémentaires. Cependant, pour avoir un contrôle plus fin de la
génération, il faut exploiter le fait que les fonctions de signature
`a -> Gen a` sont monadiques, c'est-à-dire peuvent être combinées pour produire
des générateurs complexe.

Nous allons exploiter le fait que `Gen` est un contexte monadique. Notamment,
 si on a un générateur `gen` de type `Gen a` et une fonction monadique `f` de type
`a -> Gen b` alors l'expression `gen >>= f` produit un générateur `Gen b` qui dépend
potentiellement de `gen`.

Le générateur le plus simple que l'on puisse imaginer est celui qui génère toujours
la même valeur. On cherche donc la signature `a -> Gen a` et c'est le fameux
`pure` des applicatifs  (aussi connu comme le `return` des monades).

Par exemple :

```haskell
>>> sample' $ (pure 42 :: Gen Int)
[42,42,42,42,42,42,42,42,42,42,42]

Illustrons maintenant l'utilisation du bind. Considérons comme exemple un générateur qui inverse les valeurs produites
par un générateur de nombres.

}-}

chooseInv :: Num a => Gen a -> Gen a
chooseInv gen = gen >>= (\x -> return (- x)) 

{-{

Par exemple :

```haskell
>>> sample' $ chooseInv $ chooseNat (10::Integer)
[-3,-3,-1,-3,-9,-2,-3,-2,-8,-9,-1]

**Question** : utiliser la notation `do` pour définir une fonction `chooseInv2` qui
définie exactement le même générateur.

}-}

chooseInv2 :: Num a => Gen a -> Gen a
chooseInv2 gen = do
                    a <- gen
                    (\x -> return (- x)) a
{-{

Par exemple :

```haskell
>>> sample' $ chooseInv2 $ chooseNat (10::Integer)
???????
```
}-}  

{-{

A présent, étudions la génération de couples de valeurs. On peut être étonné
de ne pas voir dans la bibliothèque de combinateur avec une signature comparable
à la suivante :

```haskell
genPair :: Gen a -> Gen b -> Gen (a, b)
```

Dans la bibliothèque tierce *checkers* il existe l'opérateur  `(>*<)` mais il est très facile de créer un tel générateur, ici en
exploitant la notation `do` :

}-}

genPair :: Gen a -> Gen b -> Gen (a, b)
genPair genFst genSnd = do
  x <- genFst
  y <- genSnd
  return (x,y)

{-{

Par exemple :

```haskell
>>> samples 5 $ genPair (chooseNat 10) (arbitrary :: Gen Bool)
???????
```
}-}

{-{

**Question** : version applicative

On peut voir que `x` et `y` sont traités de façon indépendantes, on peut même dire parallèle,
puisque on aurait pu les tirer dans un autre sens. Dans ce genre de situation, ou l'aspect
séquentiel des monades n'est pas exploité, le contexte applicatif est souvent suffisant.
A partir de cette remarque, définir un générateur `genPair2` qui génère
des couples dans un contexte applicatif.

}-}

-- genPair2 :: Applicative f => f a -> f b -> f (a, b)
genPair2 :: Gen a -> Gen b -> Gen (a, b)
genPair2 x y = (,) <$> x <*>y

{-{

Par exemple :

```haskell
>>> samples 5 $ genPair2 (chooseNat (10::Integer)) (arbitrary :: Gen Bool)
[(1,False),(7,False),(7,False),(8,True),(5,True)]

----

}-}

{-{

On peut bien sûr généraliser à tout type produit. Prenons l'exemple suivant :

}-}

data Personne = Personne { nom :: String, prenom :: String, age :: Int }
  deriving (Show, Eq)

{-{

**Question** : Définir un générateur `genPersonne` pour ce type. Pour le
nom est le prénom on indiquera une taille désirée de 10, et
pour l'age se sera entre 7 et 99 ans.
}-}

genPersonne :: Gen Personne
genPersonne = Personne <$> vectorOf  10 (choose ('a', 'z'))  <*> vectorOf 10 (choose ('a', 'z'))<*> choose (7, 99)

{-{

Par exemple :

```haskell
>>> samples 2 $ genPersonne
???????
```
}-}

{-{

Bien sûr, on peut définir des générateurs récursifs pour les types somme (et somme de produits).

Voici par exemple un générateur alternatif pour `Maybe` :

}-}

genMaybe :: Gen a -> Gen (Maybe a)
genMaybe gen = do
  x <- arbitrary -- ici on tire un booléen
  case x of
    False -> return Nothing
    True -> gen >>= (pure . Just)  -- ou  do { x <- gen ; pure (Just x) }

{-{

**Remarque** : bien sûr on aurait pu utiliser `oneof` mais on voit ici une autre possibilité.

Par exemple :

```haskell
>>> samples 6 $ genMaybe $ choose ((1, 10) ::(Integer,Integer))
[Nothing,Just 6,Nothing,Nothing,Just 1,Nothing]

}-}

{-{

**Question** : Soit le type suivant

}-}

data Geom = Point
  | Rect { longueur :: Int, largeur :: Int }
  | Circle { rayon :: Int }
  deriving (Show, Eq)

{-{

Définir un générateur `genGeom` qui tire dans 20% des cas un point,
 dans 50% des cas un rectangle de longueur dans l'intervalle (5, 60) et de largeur
 minimale 1 et maximale 40 (en faisant attention que la longueur soit au moins aussi grande que la largeur).
Dans les autres cas un cercle de rayon entre 1 et 10.

}-}

genGeom :: Gen Geom
genGeom = frequency [(20, return  Point), 
                      (50, Rect <$>choose (5, 60)<*>choose (1, 40) ), (30, choose (1, 10) >>=(pure . Circle) )]

{-{

Par exemple :

```haskell
>>> samples 3 genGeom
[Rect {longueur = 7, largeur = 18},Rect {longueur = 33, largeur = 22},Circle {rayon = 6}]

}-}

{-{

## Générateurs récursifs

On peut bien sûr implémenter un algorithme récursif de génération.

En guise d'illustration, nous pouvons définir un générateur pour nombres naturels,
 en partant du type suivant :

}-}

data Nat = Z | S Nat
  deriving (Show, Eq, Ord)

{-{

Comme tout type récursif, le principe est de générer un arbre dont les feuilles
sont des constructeurs atomiques (comme `Z` pour zéro) et les noeuds internes sont
les constructeurs récursifs (ici `S` pour successeur).

}-}

genNat :: Gen Nat
genNat = do
  m <- getSize  -- on utilise le paramètre de taille comme borne max
  n <- choose (1, m)
  genAux n Z
    where genAux :: Int -> Nat -> Gen Nat
          genAux n k | n > 0 = genAux (n-1) (S k)
                     | otherwise = return k

{-{

Par exemple :

```haskell
>>> samples 1 $ resize 10 $ genNat
[S (S (S (S (S (S (S (S (S Z))))))))]

}-}


{-{

On se rappelle que le générateur `listOf` interprète le paramètre de taille
des générateurs comme un ordre de grandeur. On peut donc définir une
variante qui interprête la taille de manière plus stricte.

**Question** : définir un générateur `listOfSize :: Int -> Gen a -> Gen [a]`
qui retourne systématiquement des listes de la taille spécifiée en argument.

}-}

listOfSize :: Gen a -> Int -> Gen [a]
listOfSize gen size = loop size
                        where loop cpt 
                                | cpt <= 0 = pure []
                                | otherwise =  liftA2 (:) gen (loop (cpt - 1)) 

{-{

Par exemple :

```haskell
>>> samples 4 $ listOfSize (chooseNat( 10 ::Integer)) 5
[[4,3,8,7,9],[7,10,5,1,3],[8,4,2,2,10],[8,2,8,9,2]]

Nous pouvons en déduire un combinateur qui utilise le paramètre de taille
implicite de la monade `Gen`.

}-}

sizedList :: Gen a -> Gen [a]
sizedList gen = do
  size <- getSize
  listOfSize gen size

{-{

Par exemple :

```haskell
>>> samples 4 $ resize 5 $ sizedList $ chooseNat ( 10 ::Integer)
[[7,8,3,7,8],[6,7,10,2,1],[5,7,9,2,10],[5,7,10,3,6]]

Il existe une autre façon d'écrire des variantes pour les générateurs de la
forme `Int -> Gen a` avec la fonction QuickCheck suivante :

```haskell
sized :: (Int -> Gen a) -> Gen a
```

Ici on peut donc écrire :

}-}

sizedList' :: Gen a -> Gen [a]
sizedList' gen = sized (listOfSize gen) 

{-{

Par exemple :

```haskell
>>> samples 4 $ resize 5 $ sizedList' $ chooseNat ( 10 ::Integer)
[[5,3,4,2,7],[4,9,4,1,10],[5,6,1,7,6],[1,9,2,3,8]]

}-}

{-{

Nous allons prendre comme dernier exemple récursif le cas des arbres binaires.
Nous partons du type suivant :

}-}

data BinTree a = Tip | Node a (BinTree a) (BinTree a)
  deriving (Show, Eq)

{-{

Nous allons supposer que la taille de l'arbre correspond au nombres
de noeuds internes `Node`.

Un générateur naif est par exemple le suivant :

}-}

genBinTreeNaive :: Gen a -> Int -> Gen (BinTree a)
genBinTreeNaive _ 0 = return Tip
genBinTreeNaive gen 1 = gen >>= \x -> return $ Node x Tip Tip
genBinTreeNaive gen size = do
  x <- gen
  lsize <- choose (0, size-1)
  l <- genBinTreeNaive gen lsize
  r <- genBinTreeNaive gen (size - lsize)
  return $ Node x l r

{-{

Par exemple :

```haskell
>>> samples 1 $ genBinTreeNaive (choose ((1, 5) ::(Integer,Integer))) 5
[Node 2 (Node 3 Tip Tip) (Node 4 (Node 2 (Node 1 Tip Tip) (Node 2 (Node 1 Tip Tip) (Node 1 Tip Tip))) (Node 5 Tip Tip))]

}-}

{-{

**Question** : Définir un générateur `GenBinTree` qui utilise le paramètre implicite de taillle
de la monade `Gen`.

}-}

genBinTree :: Gen a -> Gen (BinTree a)
genBinTree gen = do
  size <- getSize
  genBinTreeNaive gen size

{-{

Par exemple :

```haskell
>>> samples 1 $ resize 5 $ genBinTree (choose ((1, 5) ::(Integer,Integer)))
[Node 3 (Node 5 (Node 2 Tip Tip) (Node 5 Tip Tip)) (Node 4 (Node 5 (Node 2 Tip Tip) (Node 4 Tip Tip)) (Node 5 Tip Tip))]

}-}

